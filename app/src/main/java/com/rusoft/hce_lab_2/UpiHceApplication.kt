package com.rusoft.hce_lab_2

import android.app.Application
import com.rusoft.hce_lab_2.Environment.*
import com.upi.hcesdk.api.service.HceApiService


class UpiHceApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        HceApiService.initialize(
            applicationContext,
            HOST_URL,
            WALLET_ID,
            REQUESTER_ID,
            REQUESTER_KEY,
            PUBLIC_KEY_ID,
            PUBLIC_KEY_MODULUS,
            PUBLIC_KEY_EXPONENT,
            null,
            applicationContext.getDrawable(R.drawable.ic_hce)
        )

    }

}