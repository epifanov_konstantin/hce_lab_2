package com.rusoft.hce_lab_2

/*UI*/
const val DURATION_FAST = 100.toLong()
const val DURATION_VERY_FAST = 50.toLong()

const val ALPHA_INVISIBLE = 0f
const val ALPHA_DISABLED = 0.5f
const val ALPHA_VISIBLE = 1f

const val SCALE_PRESSED = 0.975f
const val SCALE_FULL = 1f

const val CLICK_ACTION_THRESHOLD = 200