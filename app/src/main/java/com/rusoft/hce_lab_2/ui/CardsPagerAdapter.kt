package com.rusoft.hce_lab_2.ui

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.lab.greenpremium.utills.LogUtil
import com.rusoft.hce_lab_2.R
import com.rusoft.hce_lab_2.data.Card


class CardsPagerAdapter(private val context: Context,
                        private val cards: List<Card>) : PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.view_card, collection, false) as ViewGroup

        val card = cards[position]
        LogUtil.i("SET DATA: $card")
        layout.findViewById<TextView>(R.id.pan).text = card.pan

        collection.addView(layout)
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.getString(position)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int = cards.size

}