package com.rusoft.hce_lab_2.ui

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.lab.greenpremium.utills.LogUtil
import com.lab.greenpremium.utills.TouchListener
import com.lab.greenpremium.utills.setTouchAnimationShrink
import com.rusoft.hce_lab_2.data.Card
import com.rusoft.hce_lab_2.data.UserModel
import com.rusoft.hce_lab_2.data.UserModel.cards
import com.upi.hcesdk.api.EnrollData
import com.upi.hcesdk.api.service.HceApiService
import com.upi.hcesdk.callback.StatusCallback
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.rusoft.hce_lab_2.R.layout.activity_main)
        setSupportActionBar(toolbar)

        pager_cards.adapter = CardsPagerAdapter(applicationContext, UserModel.cards)
        pager_cards.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageSelected(index: Int) {
                cards.forEach { card ->
                    card.isChosen = cards.indexOf(card) == index
                    LogUtil.i("card.isChosen = ${cards.indexOf(card) == index}")
                }
            }
        })

        setTouchAnimationShrink(button_hce, object : TouchListener {
            override fun onStartPressing() {
                LogUtil.e("onStartPressing")
            }

            override fun onEndPressing() {
                LogUtil.e("onEndPressing")
            }
        })

        fab.setOnClickListener { showAddCardDialog() }
    }

    private fun showAddCardDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Введите номер карты:")

        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        builder.setView(input)

        builder.setPositiveButton("OK") { _, _ -> addCard(input.text.toString()) }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    private fun addCard(pan: String) {
        HceApiService.getInstance().enroll(pan, "firebasedInstanceID", object : StatusCallback<EnrollData, String> {
            override fun success(data: EnrollData?) {
                LogUtil.e("enrolling success: $data")
            }

            override fun failure(s: String?) {
                LogUtil.e("enrolling failure: $s")
            }
        })

        UserModel.cards.add(Card(pan))
        pager_cards.adapter?.notifyDataSetChanged()
        pager_cards.setCurrentItem(UserModel.cards.lastIndex, true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(com.rusoft.hce_lab_2.R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            com.rusoft.hce_lab_2.R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

}
