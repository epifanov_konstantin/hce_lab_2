package com.rusoft.hce_lab_2;

public class Environment {

    /* ожидаем эти данные от UnionPay */
    public static String HOST_URL = "";
    public static String WALLET_ID = "";
    public static String REQUESTER_ID = "";
    public static String REQUESTER_KEY = "";
    public static String PUBLIC_KEY_ID = "";
    public static String PUBLIC_KEY_MODULUS = "";
    public static String PUBLIC_KEY_EXPONENT = "";

}
