package com.rusoft.hce_lab_2;

public class ApduConstants {

    public static final byte[] UNKNOWN_ERROR_RESPONSE = {0x6F, 0x00};

    public static final byte[] SUPPORTED_AID = {(byte) 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x10};

    public static final byte[] SELECT_PPSE = {
            0x00, // CLA
            (byte) 0xA4, // INS
            0x04, // P1
            0x00, // P2
            0x0E, // Lc
            '2', 'P', 'A', 'Y', '.', 'S', 'Y', 'S', '.', 'D', 'D', 'F', '0', '1', // Data: 2PAY.SYS.DDF01
            0x00 // Le
    };

    public static final byte[] SELECT_AID = {
            0x00, // CLA
            (byte) 0xA4, // INS
            0x04, // P1
            0x00, // P2
            0x07, // Lc
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x10, // Data: SUPPORTED_AID
            0x00 // Le
    };

    public static final byte[] READ_MAGSTRIPE_RECORDS = {0x00, (byte) 0xB2, 0x01, 0x0C, 0x00};

    public static final byte[] GET_PROCESSING_OPTIONS = {
            (byte) 0x80, // CLA
            (byte) 0xA8, // INS
            0x00, // P1
            0x00, // P2
            0x02, // Lc (variable)
            (byte) 0x83, 0x00, // Data (variable)
            0x00 // Le
    };

    public static final byte[] GET_PROCESSING_OPTIONS_RESPONSE = {
            0x77, 0x0A, // Response Message Template
            (byte) 0x82, 0x02, 0x00, 0x00, // Application Interchange Profile (AIP) - modified for MagStripe downgrade attack
            (byte) 0x94, 0x04, 0x08, 0x01, 0x01, 0x00, // Application File Locator
            (byte) 0x90, 0x00 // Status Bytes for Normal Processing
    };

    public static final byte[] COMPUTE_CRYPTOGRAPHIC_CHECKSUM = {
            (byte) 0x80, // CLA
            0x2A, // INS
            (byte) 0x8E, // P1
            (byte) 0x80, // P2
            0x04, // Lc (variable)
            0x00, 0x00, 0x00, 0x00, // Data (variable)
            0x00 // Le
    };
}
