package com.rusoft.hce_lab_2.data


object UserModel {
    val cards: MutableList<Card> = mutableListOf()

    fun getChosenCard() : Card? {
        val index = getCurrentCardIndex()
        return if (index != -1) cards[index] else null
    }

    private fun getCurrentCardIndex() : Int {
        cards.forEachIndexed { index, card ->
            if (card.isChosen) return index
        }
        return -1
    }
}